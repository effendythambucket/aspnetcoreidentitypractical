using System.Collections.Generic;
using AspNetCoreIdentityPractical.EntityFramework;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreIdentityPractical.Models;

namespace AspNetCoreIdentityPractical.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext Context;
        private UserManager<IdentityUser> UserManager;

        public HomeController(AppDbContext context, UserManager<IdentityUser> userManager){
            Context = context;
            UserManager = userManager;
        }
        
        [Authorize]
        public IActionResult Index()
        {
            ViewBag.LoggedInUser = User.Identity.Name;
            return View();
        }
        
    }
}