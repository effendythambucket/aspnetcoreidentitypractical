using System.Threading.Tasks;
using AspNetCoreIdentityPractical.EntityFramework;
using AspNetCoreIdentityPractical.Models.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace AspNetCoreIdentityPractical.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<IdentityUser> _userManager;
        private SignInManager<IdentityUser> _signInManager;
        private AppDbContext _context;
        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            AppDbContext context
        ){
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl){
            if (User.Identity.IsAuthenticated){
                return RedirectToAction("Index","Home");
            }
            ViewBag.ReturnUrl = returnUrl;
            Models.Account.LoginModel model = new Models.Account.LoginModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login(Models.Account.LoginModel model, string returnUrl){
            if (!ModelState.IsValid){
                return View(model);
            }

            IdentityUser user = _context.Users.Where(x => x.NormalizedEmail == model.Email).FirstOrDefault();
            if (user == null){
                ModelState.AddModelError("","Email is not registered");
                return View(model);
            }

            if (await _userManager.CheckPasswordAsync(user, model.Password)){
                if (!await _userManager.IsEmailConfirmedAsync(user)){
                    ModelState.AddModelError("","Email is not confirmed");
                    return View(model);
                }
                else {
                    await _signInManager.SignInAsync(user, model.RememberMe);
                    return RedirectToAction("Index","Home");
                }
            }
            else{
                ModelState.AddModelError("","Invalid username or password");
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult Register(){
            if (User.Identity.IsAuthenticated){
                return RedirectToAction("Index","Home");
            }
            RegisterModel model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model){
            if (!ModelState.IsValid){
                return View(model);
            }
            IdentityUser user = new IdentityUser(){
                UserName = model.Email,
                NormalizedUserName = model.Email.ToUpper(),
                Email = model.Email,
                NormalizedEmail = model.Email.ToUpper()
            };

            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded){
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var confirmationLink = Url.Action("ConfirmEmailAddress","Account", new {
                    token = token,
                    email = user.Email
                }, Request.Scheme);
                //lazily reuse forgot password page for testing purpose only, development must be differ page
                ViewData["PasswordResetLink"] = confirmationLink;
                return View("ForgotPasswordResult");
            }
            else {
                foreach (var error in result.Errors){
                    ModelState.AddModelError("", error.Description);
                }
                return View(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SignOut(){
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public IActionResult ForgotPassword(){
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel model){
            if (!ModelState.IsValid) return View(model);

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null){
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                ViewData["PasswordResetLink"] = Url.Action("ResetPassword","Account", new { email = model.Email , token= token }, Request.Scheme);
                return View("ForgotPasswordResult");
            }
            return View("ForgotPasswordResult");
        }

        [HttpGet]
        public IActionResult ResetPassword(string token, string email){
            if (token == null || email == null){
                ModelState.AddModelError("","Invalid password reset token");
            }
            ResetPasswordModel model = new ResetPasswordModel(){
                Email = email,
                Token = token
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model){
            if (!ModelState.IsValid) return View(model);

            IdentityUser user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null){
                IdentityResult result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);

                if (!result.Succeeded){
                    foreach (IdentityError error in result.Errors){
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }
                return RedirectToAction("Index","Home");
            }
            ModelState.AddModelError("","Invaid reset password");

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmailAddress(string token, string email){
            if (token == null || email == null){
                ModelState.AddModelError("","Invalid email confirmation");
            }

            IdentityUser user = await _userManager.FindByEmailAsync(email);
            if (user != null){
                IdentityResult result = await _userManager.ConfirmEmailAsync(user, token);
                if (result.Succeeded){
                    return RedirectToAction("Login","Account");
                }
                ModelState.AddModelError("","Invalid email confirmation");
            }

            return View("Register");

        }
    }
}