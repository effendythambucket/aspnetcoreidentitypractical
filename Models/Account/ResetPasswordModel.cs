using System.ComponentModel.DataAnnotations;

namespace AspNetCoreIdentityPractical.Models.Account
{
    public class ResetPasswordModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string ConfirmationPassword { get; set; }
        [Required]
        public string Token { get; set; }
    }
}