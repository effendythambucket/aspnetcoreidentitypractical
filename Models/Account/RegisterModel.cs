using System.ComponentModel.DataAnnotations;

namespace AspNetCoreIdentityPractical.Models.Account
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "First Name must not be empty")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name must not be empty")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email must not be empty")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password must not be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirmation Password must not be empty")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmationPassword { get; set; }
    }   
}