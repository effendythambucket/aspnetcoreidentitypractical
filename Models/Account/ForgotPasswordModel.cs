using System.ComponentModel.DataAnnotations;

namespace AspNetCoreIdentityPractical.Models.Account
{
    public class ForgotPasswordModel
    {
        [Required]
        public string Email { get; set; }
    }
}